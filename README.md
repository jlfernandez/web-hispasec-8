# Hispasec Web

Repositorio de la web de hispasec (hispasec.com)

# Instalación, creación de templates y servidor de prueba

```bash
# Instalación
git clone https://bitbucket.org/operacioncoconut/web.git --depth 1
cd web
virtualenv venv
source venv/bin/activate
npm install -g less
pip install -r requirements.txt
```

# Creación de templates

```bash
sh compilebabel.sh
```
# Servidor de pruebas

```bash
open http://127.0.0.1:8000/
python -m SimpleHTTPServer
```
## Actualización del código en desarrollo

```bash
while true; do cp -r static/. output/static; python script.py; sleep 1; done
```